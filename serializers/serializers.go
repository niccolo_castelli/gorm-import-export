package serializers

import (
	"encoding/json"
	"io/ioutil"
)

type ImportExportSerializer interface {
	ImportFile(filePath string, dst interface{}) error
	Import(src []byte, dst interface{}) error
	Export(src interface{}) ([]byte, error)
}

func NewJsonSerializer() ImportExportSerializer {
	return &jsonSerializer{}
}

type jsonSerializer struct{}

func (s *jsonSerializer) ImportFile(filePath string, dst interface{}) error {
	dat, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}
	return s.Import(dat, dst)
}

func (*jsonSerializer) Import(src []byte, dst interface{}) error {
	return json.Unmarshal(src, dst)
}

func (*jsonSerializer) Export(src interface{}) ([]byte, error) {
	return json.Marshal(src)
}
