package import_export

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"gorm-import-export/serializers"
	"io/ioutil"
	"strings"
)

var qTemplate = `INSERT INTO %s (%s) VALUES (%s) 
ON CONFLICT ON CONSTRAINT %s_pkey DO UPDATE SET 
%s
`

func NewJsonImportExport(Db *gorm.DB) *ImportExport {
	return &ImportExport{
		Serializer: serializers.NewJsonSerializer(),
		Db:         Db,
	}
}

type ImportExportTable struct {
	Name string
	Pk   string
	Data []map[string]interface{}
}

func (t *ImportExportTable) TableName() string {
	return t.Name
}

type ImportExport struct {
	Serializer serializers.ImportExportSerializer
	Db         *gorm.DB
}

func (ie *ImportExport) tableExists(tableName string) bool {
	return ie.Db.Dialect().HasTable(tableName)
}
func (ie *ImportExport) Import(filePath string) error {
	f, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}
	var tables = &[]ImportExportTable{}
	err = ie.Serializer.Import(f, tables)
	if err != nil {
		return err
	}
	for _, table := range *tables {
		if table.Name == "" || !ie.tableExists(table.Name) {
			return errors.New(fmt.Sprintf("Table %s not found", table.Name))
		}

		rowCount := len(table.Data)
		fmt.Println("IMPORT", table)

		for iR, row := range table.Data {
			colNames := make([]string, len(row))
			colPlaceholders := make([]string, len(row))
			colVals := make([]interface{}, len(row))
			updateVals := make([]string, len(row))
			var i = 0
			for name, val := range row {
				colNames[i] = name
				colVals[i] = val
				updateVals[i] = fmt.Sprintf("%s = ?", name)
				colPlaceholders[i] = "?"
				i += 1
			}

			q := fmt.Sprintf(qTemplate,
				table.Name, strings.Join(colNames, ","), strings.Join(colPlaceholders, ","), table.Name, strings.Join(updateVals, ",\n"))
			fmt.Println(q)
			colVals = append(colVals, colVals...)
			err = ie.Db.Exec(q, colVals...).Error
			fmt.Printf("row: %v/%v %s %v\n", iR, rowCount, err, row)
		}
	}

	return nil
}
func (ie *ImportExport) Export(model interface{}) (*ImportExportTable, error) {
	scope := ie.Db.NewScope(model)
	tableName := scope.TableName()
	fmt.Println(scope.PrimaryKey())

	if !ie.tableExists(tableName) {
		return nil, errors.New(fmt.Sprintf("Table %s not found", tableName))
	}

	var result []map[string]interface{}
	q := "SELECT * FROM " + tableName

	rows, err := ie.Db.Raw(q).Rows()

	cols, _ := rows.Columns()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		// Create a slice of interface{}'s to represent each column,
		// and a second slice to contain pointers to each item in the columns slice.
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i, _ := range columns {
			columnPointers[i] = &columns[i]
		}

		// Scan the result into the column pointers...
		if err := rows.Scan(columnPointers...); err != nil {
			return nil, err
		}

		// Create our map, and retrieve the value for each column from the pointers slice,
		// storing it in the map with the name of the column as the key.
		m := make(map[string]interface{})
		for i, colName := range cols {
			val := columnPointers[i].(*interface{})
			m[colName] = val
		}

		// Outputs: map[columnName:value columnName2:value2 columnName3:value3 ...]
		result = append(result, m)
	}
	table := &ImportExportTable{
		Name: tableName,
		Pk:   scope.PrimaryKey(),
		Data: result,
	}
	return table, nil
}

func (ie *ImportExport) ExportModel(model interface{}) ([]byte, error) {

	table, err := ie.Export(model)
	if err != nil {
		return nil, err
	}
	return ie.Serializer.Export(table)
}
func (ie *ImportExport) ExportModels(models ...interface{}) ([]byte, error) {
	serializedModels := make([]*ImportExportTable, len(models))
	for i, dbModel := range models {
		data, err := ie.Export(dbModel)
		if err != nil {
			return nil, err
		}
		serializedModels[i] = data
	}

	return ie.Serializer.Export(serializedModels)
}
